<?php

use CyberTerm\Classes\Game;

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
define('VENDOR', ROOT . 'vendor' . DIRECTORY_SEPARATOR);

require VENDOR . 'autoload.php';

$game = new Game();
$game->play();
