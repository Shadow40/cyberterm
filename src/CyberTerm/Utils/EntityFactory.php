<?php

namespace CyberTerm\Utils;

use CyberTerm\Classes\Building;
use CyberTerm\Classes\HealthPill;
use CyberTerm\Classes\NPC;
use CyberTerm\Classes\Weapon;
use Exception;

class EntityFactory
{
    public static $minGeneratedNPCs = 3;
    public static $maxGeneratedNPCs = 6;
    public static $minGeneratedBuildings = 5;
    public static $maxGeneratedBuildings = 10;
    public static $minGeneratedWeapons = 1;
    public static $maxGeneratedWeapons = 2;
    public static $minGeneratedHealthPills = 1;
    public static $maxGeneratedHealthPills = 2;

    /**
     * Generates random NPCs
     *
     * @param int $count
     * @return NPC[]
     */
    public static function generateNPCs($count = 2): array
    {
        $NPCs = [];
        $maxNPCs = $count;

        try {
            $maxFriendlyNPCs = random_int(1, $maxNPCs / 2);
        } catch (Exception $e) {
            $maxFriendlyNPCs = mt_rand(1, $maxNPCs / 2);
        }

        for ($i = 0; $i < $maxNPCs; $i++) {
            $npc = new NPC();
            try {
                $npc->setMoney(random_int(0, 30));
            } catch (Exception $e) {
                $npc->setMoney(mt_rand(0, 30));
            }

            if ($maxFriendlyNPCs) {
                $npc->setIsEnemy(false);
                $maxFriendlyNPCs--;
            }

            $npc->addWeapon(self::generateWeapons()[0]);

            $NPCs[] = $npc;
        }

        return $NPCs;
    }

    /**
     * Generates buildings
     *
     * @param int $count
     * @return Building[]
     */
    public static function generateBuildings($count = 2): array
    {
        $buildings = [];

        for ($i = 0; $i < $count; $i++) {
            $building = new Building();
            $buildings[] = $building;
        }

        return $buildings;
    }

    /**
     * Generates random weapons
     *
     * @param int $count
     * @return Weapon[]
     */
    public static function generateWeapons($count = 1): array
    {
        $weapons = [];

        for ($i = 0; $i < $count; $i++) {
            try {
                $weaponsList = [
                    new Weapon('Sabre', random_int(10, 15)),
                    new Weapon('Pistol', random_int(18, 22)),
                    new Weapon('Laser', random_int(30, 35)),
                ];
            } catch (Exception $e) {
                $weaponsList = [
                    new Weapon('Sabre', mt_rand(10, 15)),
                    new Weapon('Pistol', mt_rand(18, 22)),
                    new Weapon('Laser', mt_rand(30, 35)),
                ];
            }

            $weapons[] = $weaponsList[array_rand($weaponsList)];
        }

        return $weapons;
    }

    /**
     * Generates random health pills
     *
     * @param int $count
     * @return array
     */
    public static function generateHealthPills($count = 1): array
    {
        $healthPills = [];

        for ($i = 0; $i < $count; $i++) {
            $healthPills[] = new HealthPill();
        }

        return $healthPills;
    }
}
