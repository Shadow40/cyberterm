<?php

namespace CyberTerm\Utils;

use RuntimeException;
use League\CLImate\CLImate;

class IO
{
    private static $climateInstance;

    /**
     * Returns a ClImate instance
     *
     * @return CLImate
     */
    public static function climate(): CLImate
    {
        if (!isset(self::$climateInstance)) {
            self::$climateInstance = new CLImate();
        }

        return self::$climateInstance;
    }

    /**
     * Writes a dialog line progressively, asking then the user to continue
     *
     * @param string $text
     * @param bool $displayContinue
     */
    public static function writeDialog($text, $displayContinue = true): void
    {
        for ($i = 0, $iMax = strlen($text); $i < $iMax; $i++) {
            self::climate()->inline($text[$i]);
            usleep(50000);
        }

        if ($displayContinue) {
            self::climate()->lightGreen()->inline(' [Press ENTER to continue]');
        }

        self::climate()->input('')->prompt();
        self::climate()->br();
    }

    /**
     * Loads the game state from the save file
     *
     * @param array $needles
     * @return array
     * @throws RuntimeException
     */
    public static function loadSaveFile($needles = null): array
    {
        if ($needles !== null && !is_array($needles)) {
            throw new RuntimeException('Provided data to load is not an array.');
        }

        $saveDirectory = __DIR__ . '/../../../savedata/';

        if (!is_dir($saveDirectory) || !file_exists($saveDirectory . 'save.txt')) {
            try {
                return self::saveGame([]);
            } catch (RuntimeException $e) {
            }
        }

        $savedData = json_decode(file_get_contents($saveDirectory . 'save.txt'), true);

        if ($savedData !== null) {
            if ($needles !== null && is_array($needles)) {
                $intersect = array_intersect_key($savedData, array_flip($needles));

                if (!count($intersect)) {
                    throw new RuntimeException('Provided values to load do not exist in save file.');
                }

                return $intersect;
            }
            return $savedData;
        }
        return self::saveGame([]);
    }

    /**
     * Saves data in a save file
     *
     * @param $data
     * @param bool $force
     * @return array
     * @throws RuntimeException
     */
    public static function saveGame($data, $force = false): array
    {
        if (!is_array($data)) {
            throw new RuntimeException('Provided data to save is not an array.');
        }

        $saveDirectory = __DIR__ . '/../../../savedata/';

        if (!is_dir($saveDirectory)) {
            mkdir($saveDirectory);
        }

        if (!$force && is_array($data) && count($data)) {
            try {
                $savedData = self::loadSaveFile();
            } catch (RuntimeException $e) {
            }

            $saveData = !isset($savedData) ? $data : array_merge($savedData, $data);
        } else {
            $saveData = $data;
        }

        file_put_contents($saveDirectory . 'save.txt', json_encode($saveData, JSON_PRETTY_PRINT));

        return $data;
    }
}
