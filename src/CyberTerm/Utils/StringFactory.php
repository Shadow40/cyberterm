<?php

namespace CyberTerm\Utils;

class StringFactory
{
    /**
     * Generate name from a name list file
     *
     * @return string
     */
    public static function generateName(): string
    {
        $names = file('src/resources/names.txt');
        return rtrim($names[array_rand($names)]);
    }

    /**
     * Method used when the enemy die
     * @return string
     */
    public static function generateLastWordsQuote(): string
    {
        $quotes = file('src/resources/die_quotes.txt');
        return rtrim($quotes[array_rand($quotes)]);
    }
}
