<?php

namespace CyberTerm\Classes;

class HealthPill extends Entity
{
    private static $MIN_AMOUNT = 20;
    private static $MAX_AMOUNT = 50;
    private static $HEALTH_PILL_CHARACTER = '♥';
    /** @var int */
    private $amount;

    /**
     * HealthPill constructor.
     * @param $amount
     */
    public function __construct($amount = null)
    {
        if ($amount === null) {
            $this->amount = $this->randomAmount();
        } else {
            $this->amount = $amount;
        }
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }


    private function randomAmount(): int
    {
        try {
            return random_int(self::$MIN_AMOUNT, self::$MAX_AMOUNT);
        } catch (\Exception $e) {
            return mt_rand(self::$MIN_AMOUNT, self::$MAX_AMOUNT);
        }
    }

    public function getPrintCharacter(): string
    {
        return "\e[35;44m" . self::$HEALTH_PILL_CHARACTER . "\e[0m";
    }

    public function __toString()
    {
        return $this->getPrintCharacter();
    }
}
