<?php

namespace CyberTerm\Classes;

class Map
{
    private $map;
    private $sizeX;
    private $sizeY;
    private static $PLAIN_CHARACTER = '█';

    /**
     * Map constructor.
     * @param int $sizeX
     * @param int $sizeY
     */
    public function __construct($sizeX = 60, $sizeY = 20)
    {
        $this->sizeX = $sizeX;
        $this->sizeY = $sizeY;

        for ($i = 0; $i < $sizeY; $i++) {
            for ($j = 0; $j < $sizeX; $j++) {
                // Border check
                if ($i === 0 || $j === 0 || $i === $sizeY - 1 || $j === $sizeX - 1) {
                    $this->map[$i][$j] = new Wall(new Coordinate($i, $j));
                } else {
                    $this->map[$i][$j] = $this->getPlainCharacter();
                }
            }
        }
    }

    /**
     * Return the entity at a given coordinate
     *
     * @param Coordinate $coordinates
     * @return mixed
     */
    public function getAtCoords(Coordinate $coordinates)
    {
        return $this->map[$coordinates->getY()][$coordinates->getX()];
    }

    /**
     * Sets a given entity at a coordinate
     *
     * @param Coordinate $coordinates
     * @param $char
     */
    public function setAtCoords(Coordinate $coordinates, $char): void
    {
        $this->map[$coordinates->getY()][$coordinates->getX()] = $char;
    }

    /**
     * Randomly places an entity on the map, optionally with given coordinates
     *
     * @param Entity $entity
     * @param Coordinate|null $coordinates
     */
    public function put(Entity $entity, Coordinate $coordinates = null): void
    {
        if ($coordinates === null) {
            do {
                try {
                    $coordinates = new Coordinate(random_int(0, $this->getSizeX() - 1), random_int(0, $this->getSizeY() - 1));
                } catch (\Exception $e) {
                    $coordinates = new Coordinate(mt_rand(0, $this->getSizeX() - 1), mt_rand(0, $this->getSizeY() - 1));
                }

            } while ($this->willCollide($coordinates));
        }

        $entity->setPosition($coordinates);
        $this->setAtCoords($entity->getPosition(), $entity);
    }

    /**
     * Moves an entity to a given position
     *
     * @param Entity $entity
     * @param Coordinate $position
     */
    public function move(Entity $entity, Coordinate $position): void
    {
        $this->setAtCoords($entity->getPosition(), $this->getPlainCharacter());
        $entity->setPosition($position);
        $this->setAtCoords($entity->getPosition(), $entity);
    }

    /**
     * Draws the map
     */
    public function draw(): void
    {
        // Clear screen ANSI code
        $buffer = "\e[H\e[2J";

        foreach ($this->map as $rowIndex => $rowValue) {
            foreach ($rowValue as $columnIndex => $columnValue) {
                $buffer .= $this->getAtCoords(new Coordinate($columnIndex, $rowIndex));
            }

            $buffer .= "\n";
        }

        echo $buffer . "\n";
    }

    /**
     * Returns the X size of the map
     *
     * @return int
     */
    public function getSizeX(): int
    {
        return $this->sizeX;
    }

    /**
     * Returns the Y size of the map
     *
     * @return int
     */
    public function getSizeY(): int
    {
        return $this->sizeY;
    }

    /**
     * Returns the colored plain (empty) character
     *
     * @return string
     */
    public function getPlainCharacter(): string
    {
        return "\e[34m" . self::$PLAIN_CHARACTER . "\e[0m";
    }

    /**
     * Checks collision and returns the corresponding entity
     *
     * @param Coordinate $coordinates
     * @return bool|Entity
     */
    public function willCollide(Coordinate $coordinates)
    {
        $entity = $this->getAtCoords($coordinates);

        return $entity instanceof Entity ? $entity : false;
    }
}
