<?php

namespace CyberTerm\Classes;

abstract class Character extends Entity
{
    protected $name;
    protected $experience;
    protected $health;
    /** @var Weapon[] */
    protected $weapons;
    protected $money;
    protected static $AVATAR_CHARACTER = '☻';
    public static $MAX_HEALTH = 100;

    /**
     * Character constructor.
     * @param $name
     * @param int $experience
     * @param int $health
     * @param array $weapons
     * @param int $money
     */
    public function __construct($name, $experience = 1, $health = null, $weapons = [], $money = 0)
    {
        $this->name = $name;
        $this->experience = $experience;
        $this->health = $health ?? self::$MAX_HEALTH;
        $this->weapons = $weapons;
        $this->money = $money;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getExperience(): int
    {
        return $this->experience;
    }

    /**
     * @param int $experience
     */
    public function setExperience($experience): void
    {
        $this->experience = $experience;
    }

    /**
     * @param int $experience
     */
    public function addExperience($experience): void
    {
        $this->experience += $experience;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @param int $health
     */
    public function setHealth($health): void
    {
        $this->health = $health;
    }

    /**
     * Adds health
     *
     * @param int $health
     */
    public function addHealth($health): void
    {
        $this->health = $this->health + $health > self::$MAX_HEALTH ? self::$MAX_HEALTH : $this->health + $health;
    }

    /**
     * Removes health
     *
     * @param int $health
     */
    public function removeHealth($health): void
    {
        $this->health = $this->health - $health < 0 ? 0 : $this->health - $health;
    }

    /**
     * @return Weapon[]
     */
    public function getWeapons(): array
    {
        return $this->weapons;
    }

    /**
     * @param Weapon[] $weapons
     */
    public function setWeapons($weapons): void
    {
        $this->weapons = $weapons;
    }

    /**
     * Adds a weapon
     *
     * @param $weapon
     */
    public function addWeapon($weapon): void
    {
        $this->weapons[] = $weapon;
    }

    /**
     * Removes a weapon
     *
     * @param $weapon
     */
    public function removeWeapon($weapon): void
    {
        $index = array_search($weapon, $this->weapons, true);

        if ($index !== false) {
            unset($this->weapons[$index]);
            $this->weapons = array_values($this->weapons);
        }
    }

    /**
     * Returns a random weapon
     *
     * @return Weapon
     */
    public function randomWeapon(): Weapon
    {
        return $this->weapons[array_rand($this->weapons)];
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     */
    public function setMoney(int $money): void
    {
        $this->money = $money;
    }

    /**
     * @param int $money
     */
    public function addMoney(int $money): void
    {
        $this->money += $money;
    }

    /**
     * @param int $money
     */
    public function removeMoney(int $money): void
    {
        $this->money -= $money;
    }
}
