<?php

namespace CyberTerm\Classes;

use CyberTerm\Utils\EntityFactory;

class Building extends Entity
{
    private static $WEAPON_RATE = 0.2;
    private $BUILDING_CHARACTER = '⌂';
    private $money;
    private $weapon;
    private $looted;

    /**
     * Building constructor.
     * @param Coordinate $position
     */
    public function __construct(Coordinate $position = null)
    {
        $this->position = $position;

        try {
            $this->money = random_int(0, 10);
        } catch (\Exception $e) {
            $this->money = mt_rand(0, 10);
        }

        $this->looted = false;

        try {
            $hasWeapon = random_int(1, 1 / self::$WEAPON_RATE) === 1;
        } catch (\Exception $e) {
            $hasWeapon = mt_rand(1, 1 / self::$WEAPON_RATE) === 1;
        }

        if ($hasWeapon) {
            $this->weapon = EntityFactory::generateWeapons()[0];
        } else {
            $this->weapon = null;
        }
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     */
    public function setMoney(int $money): void
    {
        if ($money === 0) {
            $this->looted = true;
        }

        $this->money = $money;
    }

    /**
     * @return Weapon
     */
    public function getWeapon(): ?Weapon
    {
        return $this->weapon;
    }

    /**
     * @param Weapon $weapon
     */
    public function setWeapon(Weapon $weapon = null): void
    {
        $this->weapon = $weapon;
    }

    /**
     * @return bool
     */
    public function isLooted(): bool
    {
        return $this->looted;
    }

    public function getPrintCharacter(): string
    {
        return "\e[37;1;44m" . $this->BUILDING_CHARACTER . "\e[0m";
    }

    public function __toString()
    {
        return $this->getPrintCharacter();
    }
}
