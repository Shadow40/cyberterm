<?php

namespace CyberTerm\Classes;

use CyberTerm\Interfaces\Displayable;
use CyberTerm\Interfaces\Positionable;

abstract class Entity implements Positionable, Displayable
{
    /**
     * @var Coordinate
     */
    protected $position;

    /**
     * @return Coordinate
     */
    public function getPosition(): Coordinate
    {
        return $this->position;
    }

    /**
     * @param Coordinate $newPosition
     */
    public function setPosition(Coordinate $newPosition): void
    {
        $this->position = $newPosition;
    }

    /**
     * @param Coordinate $difference
     */
    public function move(Coordinate $difference): void
    {
        $this->position->setX($this->position->getX() + $difference->getX());
        $this->position->setY($this->position->getY() + $difference->getY());
    }
}
