<?php

namespace CyberTerm\Classes;

class Weapon extends Entity
{
    private $name;
    private $damage;
    private static $WEAPON_CHARACTER = '¬';

    /**
     * Weapon constructor.
     * @param string $name
     * @param int $damage
     * @param Coordinate $position
     */
    public function __construct($name, $damage, Coordinate $position = null)
    {
        $this->name = $name;
        $this->damage = $damage;
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getDamage(): int
    {
        return $this->damage;
    }

    /**
     * @param int $damage
     */
    public function setDamage($damage): void
    {
        $this->damage = $damage;
    }

    public function getPrintCharacter(): string
    {
        return "\e[37;1;44m" . self::$WEAPON_CHARACTER . "\e[0m";
    }

    public function __toString()
    {
        return $this->getPrintCharacter();
    }
}
