<?php

namespace CyberTerm\Classes;

class Player extends Character
{
    public static $KILL_XP = 50;

    /**
     * @var int
     */
    private $age;
    private $lastWords;

    /**
     * Player constructor.
     * @param $name
     * @param int $experience
     * @param int $health
     * @param array $weapons
     * @param int $age
     * @param int $money
     */
    public function __construct($name, $experience = 0, $health = 100, $weapons = [], $age = 25, $money = 10)
    {
        parent::__construct($name, $experience, $health, $weapons, $money);
        $this->age = $age;
        $this->position = new Coordinate(5, 2);
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getLastWords(): string
    {
        return $this->lastWords;
    }

    /**
     * @param string $lastWords
     */
    public function setLastWords($lastWords): void
    {
        $this->lastWords = $lastWords;
    }

    /**
     * Returns the colored player character to be shown on map
     *
     * @return string
     */
    public function getPrintCharacter(): string
    {
        return "\e[44m" . self::$AVATAR_CHARACTER . "\e[0m";
    }

    /**
     * __toString override
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getPrintCharacter();
    }
}
