<?php

namespace CyberTerm\Classes;

use CyberTerm\Utils\StringFactory;
use Exception;

class NPC extends Character
{
    private $isEnemy;
    /**
     * Move probability (x/1)
     *
     * @var float
     */
    private static $moveRate = 0.2;

    /**
     * NPC constructor.
     * @param Coordinate $position
     * @param int $experience
     * @param int $health
     * @param array $attacks
     * @param int $money
     * @param bool $isEnemy
     */
    public function __construct(Coordinate $position = null, $experience = 1, $health = 100, $attacks = [], $money = 0, $isEnemy = true)
    {
        parent::__construct(StringFactory::generateName(), $experience, $health, $attacks, $money);
        $this->isEnemy = $isEnemy;
        $this->position = $position;
        $this->money = $money;
    }

    /**
     * @return bool isEnemy
     */
    public function getIsEnemy(): bool
    {
        return $this->isEnemy;
    }

    /**
     * @param bool $isEnemy
     */
    public function setIsEnemy($isEnemy): void
    {
        $this->isEnemy = $isEnemy;
    }

    /**
     * Moves randomly the NPC on the map, checking for collisions
     *
     * @param Map $map
     * @param bool $force
     */
    public function autoMove(Map $map, $force = false): void
    {
        // Don't always move
        try {
            $willMove = random_int(1, 1 / self::$moveRate) === 1;
        } catch (Exception $e) {
            $willMove = true;
        }

        if ($force || $willMove) {
            $directionArray = [
                // UP
                new Coordinate($this->getPosition()->getX(), $this->getPosition()->getY() - 1),
                // DOWN
                new Coordinate($this->getPosition()->getX(), $this->getPosition()->getY() + 1),
                // LEFT
                new Coordinate($this->getPosition()->getX() - 1, $this->getPosition()->getY()),
                // RIGHT
                new Coordinate($this->getPosition()->getX() + 1, $this->getPosition()->getY()),
            ];

            $futurePos = $directionArray[array_rand($directionArray)];

            if (!$map->willCollide($futurePos)) {
                $map->move($this, $futurePos);
            } else {
                // Retry if the the NPC collides
                $this->autoMove($map, true);
            }
        }
    }

    /**
     * Returns the character icon colored based on the friendliness
     *
     * @return string
     */
    public function getPrintCharacter(): string
    {
        return ($this->isEnemy ? $this->getEnemyCharacter() : $this->getFriendlyCharacter());
    }

    /**
     * Returns the enemy colored map icon
     *
     * @return string
     */
    public function getEnemyCharacter(): string
    {
        return "\e[31;44m" . self::$AVATAR_CHARACTER . "\e[0m";
    }

    /**
     * Returns the friendly colored map icon
     *
     * @return string
     */
    public function getFriendlyCharacter(): string
    {
        return "\e[32;44m" . self::$AVATAR_CHARACTER . "\e[0m";
    }

    public function __toString()
    {
        return $this->getPrintCharacter();
    }
}
