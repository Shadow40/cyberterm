<?php

namespace CyberTerm\Classes;

class Wall extends Entity
{
    private static $WALL_CHARACTER = '▓';

    /**
     * Wall constructor.
     * @param Coordinate $position
     */
    public function __construct(Coordinate $position)
    {
        $this->position = $position;
    }

    /**
     * Returns the colored wall character to be shown on map
     *
     * @return string
     */
    public function getPrintCharacter(): string
    {
        return "\e[37;1;44m" . self::$WALL_CHARACTER . "\e[0m";
    }

    /**
     * __toString override
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getPrintCharacter();
    }
}
