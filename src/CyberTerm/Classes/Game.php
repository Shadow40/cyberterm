<?php

namespace CyberTerm\Classes;

use CyberTerm\Utils\EntityFactory;
use CyberTerm\Utils\IO;
use CyberTerm\Utils\StringFactory;
use RuntimeException;

class Game
{
    /** @var UI */
    private $UI;
    private $seenIntro;
    private $skipMainMenuAnim;
    private $inGame;
    private $inCombat;
    private $gameJustSaved;
    private $killedEveryEnemy;
    private $ended;
    /** @var NPC[] */
    private $NPCs;
    /** @var Building[] */
    private $buildings;
    /** @var Weapon[] */
    private $weapons;
    /** @var HealthPill[] */
    private $healthPills;
    /** @var Map */
    private $map;
    /** @var Entity|NPC|Building|Weapon|HealthPill */
    private $collidingEntity;
    /** @var Player */
    private $player;

    /**
     * Game constructor.
     */
    public function __construct()
    {
        $this->UI = new UI();
        $this->seenIntro = false;
        $this->skipMainMenuAnim = false;
        $this->gameJustSaved = false;
        $this->inGame = false;
        $this->inCombat = false;
        $this->killedEveryEnemy = false;
        $this->ended = false;
        $this->NPCs = [];
        $this->buildings = [];
        $this->weapons = [];
        $this->healthPills = [];
    }

    /**
     * Initiate game
     */
    public function play(): void
    {
        IO::climate()->clear();
        $this->loadGameState();

        // Game loop
        while (!$this->hasEnded()) {
            $this->handleMainLoop();
        }
    }

    private function handleMainLoop(): void
    {
        if (!$this->inGame) {
            if (!$this->skipMainMenuAnim) {
                $this->UI->showStartScreen(!$this->seenIntro);
                $this->skipMainMenuAnim = true;
            }

            if ($this->killedEveryEnemy) {
                $this->UI->showEndGameScreen();
            }

            if ($this->player === null || $this->player->getHealth() !== 0) {
                $this->showMainMenu();
            } else {
                $this->UI->showDeathScreen($this->player);
                $this->loadGameState();
            }

        } else {
            $this->initWorld();

            while ($this->inGame) {
                // Automove NPCs
                array_walk($this->NPCs, function (NPC $npc) {
                    if ($this->collidingEntity !== $npc) {
                        $npc->autoMove($this->map);
                    }
                });

                $this->map->draw();

                if (!$this->inCombat) {

                    $this->handlePlayerCollision();

                    if (!$this->inCombat) {
                        $this->handleUserInput($this->map);
                    }
                } else {
                    $this->inCombat = false;

                    if ($this->killedEveryEnemy) {
                        $this->inGame = false;
                    }
                }
            }
        }
    }

    /**
     * Ends the game
     */
    public function end(): void
    {
        $this->ended = true;
        IO::climate()->clear();
    }

    /**
     * Shows the main menu
     */
    private function showMainMenu(): void
    {
        $response = $this->UI->showMainMenu(isset($this->player));

        if (isset($this->player)) {
            switch ($response) {
                case 1:
                    $this->ContinueGame();
                    break;
                case 2:
                    $input = IO::climate()->red()->bold()->confirm('This will reset your progression, are you sure?');

                    if ($input->confirmed()) {
                        try {
                            IO::saveGame(['seenIntro' => true], true);
                        } catch (RuntimeException $e) {
                        }

                        $this->playNewGame();
                    } else {
                        $this->showMainMenu();
                    }
                    break;
                case 3:
                    $this->confirmExit();
                    break;
            }
        } else {
            switch ($response) {
                case 1:
                    $this->playNewGame();
                    break;
                case 2:
                    $this->confirmExit();
                    break;
            }
        }
    }

    /**
     * Starts a new game
     */
    private function playNewGame(): void
    {
        $this->UI->startNewGame($this);
        $this->inGame = true;
    }

    /**
     * Directly go ingame
     */
    private function continueGame(): void
    {
        $this->inGame = true;
        $this->play();
    }

    /**
     * Handles player collisions
     */
    private function handlePlayerCollision(): void
    {
        if ($this->collidingEntity !== null) {
            switch (true) {
                case $this->collidingEntity instanceof NPC:
                    if ($this->collidingEntity->getIsEnemy() && count($this->player->getWeapons())) {
                        IO::climate()->lightRed()->inline("{$this->collidingEntity->getName()}: \"Don't give me that look, or i'll kick you in the nuts!\"");
                        IO::climate()->input('')->prompt();

                        $this->enterCombat($this->collidingEntity);

                        if ($this->player->getHealth() === 0) {
                            $this->inGame = false;
                        }
                    } elseif ($this->collidingEntity->getIsEnemy()) {
                        IO::climate()->lightRed($this->collidingEntity->getName() . ' : "I won\'t fight someone who does not have a weapon, go find one."');
                        IO::climate()->br();
                    } else {
                        IO::climate()->lightGreen($this->collidingEntity->getName() . ' : "Hello there, beware of the wanderers in this region, take care."');
                        IO::climate()->br();
                    }
                    break;
                case $this->collidingEntity instanceof Building:
                    IO::climate()->out('This building seems empty, but I can still hear strange noises.');

                    if (!$this->collidingEntity->isLooted()) {
                        $money = $this->collidingEntity->getMoney();
                        $weapon = $this->collidingEntity->getWeapon();

                        if ($money) {
                            $this->player->addMoney($money);
                            $this->collidingEntity->setMoney(0);
                            IO::climate()->lightGreen("You found $money coins!");
                        }

                        if ($weapon !== null) {
                            $this->player->addWeapon($weapon);
                            $this->collidingEntity->setWeapon(null);
                            IO::climate()->lightGreen("You just found a \"{$weapon->getName()}\"! The weapon was added to your inventory");
                        }

                        if (!$money && $weapon === null) {
                            IO::climate()->out('You didn\'t find anything.');
                        }
                    } else {
                        IO::climate()->out('You already took everything!');
                    }

                    IO::climate()->br();
                    break;
                case $this->collidingEntity instanceof Weapon:
                    IO::climate()->lightGreen("You just found a \"{$this->collidingEntity->getName()}\"! The weapon was added to your inventory");
                    IO::climate()->br();
                    $this->player->addWeapon($this->collidingEntity);

                    // Remove weapon reference
                    $this->map->setAtCoords($this->collidingEntity->getPosition(), $this->map->getPlainCharacter());
                    $this->collidingEntity = null;
                    break;
                case $this->collidingEntity instanceof HealthPill:
                    if ($this->player->getHealth() < Player::$MAX_HEALTH) {
                        $oldHealth = $this->player->getHealth();
                        $this->player->addHealth($this->collidingEntity->getAmount());
                        $healthDiff = $this->player->getHealth() - $oldHealth;
                        IO::climate()->lightGreen("You just found a health pill! You gained $healthDiff health points!");
                        IO::climate()->br();

                        // Remove weapon reference
                        $this->map->setAtCoords($this->collidingEntity->getPosition(), $this->map->getPlainCharacter());
                        $this->collidingEntity = null;
                    } else {
                        IO::climate()->lightGreen("You just found a health pill! But you don't need it!");
                        IO::climate()->br();
                    }
                    break;
            }
        }
    }

    /**
     * Enters combat mode with an enemy
     *
     * @param NPC $enemy
     */
    private function enterCombat(NPC $enemy): void
    {
        $this->inCombat = true;
        $playerTurn = true;
        $combatJustStarted = true;
        IO::climate()->output->get('buffer')->clean();
        IO::climate()->clear();
        IO::climate()->animation('combat-enter')->speed(50)->run();

        while (($enemy->getHealth() > 0 && $this->player->getHealth() > 0) || $this->collidingEntity !== null) {
            IO::climate()->clear();
            IO::climate()->draw('combat');
            $rightStr = str_pad("{$this->player->getName()} - Health: {$this->player->getHealth()}", 30, ' ');
            $leftStr = str_pad("{$enemy->getName()} - Health: {$enemy->getHealth()}", 30, ' ', STR_PAD_LEFT);
            IO::climate()->lightYellow($rightStr . $leftStr);
            IO::climate()->br();

            if ($combatJustStarted) {
                IO::climate()->red("{$enemy->getName()}: \"Prepare to die!\"");
                $combatJustStarted = false;
            }

            if ($this->player->getHealth() === 0) {
                IO::climate()->lightRed("You have been killed by {$enemy->getName()}!");
                IO::climate()->br();
                $this->player->setLastWords(IO::climate()->input("{$enemy->getName()}: \"What are your last words?\":")->prompt());
                $this->collidingEntity = null;
            } elseif ($enemy->getHealth() === 0) {
                IO::climate()->lightRed($enemy->getName() . '\'s last words : "' . StringFactory::generateLastWordsQuote() . '"');
                IO::climate()->input('')->prompt();
                IO::climate()->lightRed("{$enemy->getName()} died.");
                IO::climate()->input('')->prompt();

                // Loot body
                if ($enemy->getMoney()) {
                    IO::climate()->lightGreen("Looting {$enemy->getName()}'s body gave you {$enemy->getMoney()} coins!");
                    $this->player->addMoney($enemy->getMoney());
                } else {
                    IO::climate()->lightGreen("Looting {$enemy->getName()}'s body gave you nothing.");
                }

                $this->player->addExperience(Player::$KILL_XP);
                IO::climate()->lightGreen('Player gained ' . Player::$KILL_XP . " XP! (Total: {$this->player->getExperience()} XP)");
                IO::climate()->input('')->prompt();

                // Remove enemy NPC references
                $this->map->setAtCoords($enemy->getPosition(), $this->map->getPlainCharacter());
                $this->collidingEntity = null;

                $key = array_search($enemy, $this->NPCs, true);
                if ($key !== false) {
                    unset($this->NPCs[$key]);
                    $this->NPCs = array_values($this->NPCs);
                }

                $enemies = array_filter($this->NPCs, function (NPC $npc) {
                    return $npc->getIsEnemy();
                });

                $this->killedEveryEnemy = count($enemies) === 0;
            } else {
                // Display combat information
                $buffer = IO::climate()->output->get('buffer')->get();

                if ($buffer) {
                    IO::climate()->inline($buffer);
                    IO::climate()->output->get('buffer')->clean();
                    IO::climate()->input('')->prompt();
                } else {
                    IO::climate()->br();
                }

                if ($playerTurn) {
                    IO::climate()->lightGreen('Inventory weapons:');

                    // Make array start index at 1 (for prompt and display)
                    $playerWeapons = array_values($this->player->getWeapons());
                    array_unshift($playerWeapons, '');
                    unset($playerWeapons[0]);

                    foreach ($playerWeapons as $index => $weapon) {
                        IO::climate()->out("<light_green>[$index]</light_green> - {$weapon->getName()} ({$weapon->getDamage()} dmg)");
                    }

                    IO::climate()->br();
                    $input = IO::climate()->input('Please select your weapon');
                    $input->accept(array_keys($playerWeapons), true);
                    $answer = $input->prompt();
                    $playerChosenWeapon = $playerWeapons[$answer];

                    $enemy->removeHealth($playerChosenWeapon->getDamage());

                    IO::climate()->to('buffer')->lightGreen("You inflicted {$playerChosenWeapon->getDamage()} damage to {$enemy->getName()}!");
                    $playerTurn = !$playerTurn;
                } else {
                    $enemyWeapon = $enemy->getWeapons()[array_rand($enemy->getWeapons())];
                    $this->player->removeHealth($enemyWeapon->getDamage());
                    IO::climate()->to('buffer')->red("{$enemy->getName()} used his/her {$enemyWeapon->getName()} to inflict you {$enemyWeapon->getDamage()} damage points!");
                    $playerTurn = !$playerTurn;
                }
            }
        }

        IO::climate()->clear();
        IO::climate()->animation('combat-leave')->speed(50)->run();
    }

    /**
     * Confirms the game exit
     */
    private function confirmExit(): void
    {
        $input = IO::climate()->red()->bold()->confirm('Exit the game?');

        if ($input->confirmed()) {
            $this->end();
        } else {
            $this->showMainMenu();
        }
    }

    /**
     * Confirms exit to the main menu
     */
    private function confirmExitToMenu(): void
    {
        $goToMainMenuInput = IO::climate()->red()->bold()->confirm('Go to main menu?');

        if ($goToMainMenuInput->confirmed()) {
            if (!$this->gameJustSaved) {
                $saveGameInput = IO::climate()->red()->bold()->confirm('Would you like to save the game?');
                $this->gameJustSaved = false;

                if ($saveGameInput->confirmed()) {
                    try {
                        $this->saveGameState();
                    } catch (RuntimeException $e) {
                    }
                }
            }

            $this->inGame = false;
        }
    }

    /**
     * Load values from the savegame
     */
    private function loadGameState(): void
    {
        try {
            $savedData = IO::loadSaveFile();
            $seenIntro = count($savedData) ? $savedData['seenIntro'] : false;

            $wasInGame = array_key_exists('playerName', $savedData) && array_key_exists('playerHealth', $savedData)
                && array_key_exists('playerAge', $savedData) && array_key_exists('playerMoney', $savedData)
                && array_key_exists('playerExperience', $savedData) && array_key_exists('playerPosition', $savedData);

            if ($wasInGame) {
                $this->player = new Player($savedData['playerName']);
                $this->player->setHealth($savedData['playerHealth']);
                $this->player->setAge($savedData['playerAge']);
                $this->player->setMoney($savedData['playerMoney']);
                $this->player->setExperience($savedData['playerExperience']);
                $this->player->setPosition(new Coordinate($savedData['playerPosition']['x'], $savedData['playerPosition']['y']));
            }

            if ($wasInGame && array_key_exists('playerWeapons', $savedData)) {
                foreach ($savedData['playerWeapons'] as $playerWeapon) {
                    $this->player->addWeapon(new Weapon($playerWeapon['name'], $playerWeapon['damage']));
                }
            }

        } catch (RuntimeException $e) {
            $seenIntro = false;
        }

        $this->seenIntro = is_bool($seenIntro) ? $seenIntro : false;
    }

    /**
     * Saves player data
     */
    public function saveGameState(): void
    {
        $playerWeaponsData = [];

        /** @var Weapon $weapon */
        foreach ($this->player->getWeapons() as $weapon) {
            $playerWeaponsData[] = ['name' => $weapon->getName(), 'damage' => $weapon->getDamage()];
        }

        IO::saveGame([
            'playerPosition'   => ['x' => $this->player->getPosition()->getX(), 'y' => $this->player->getPosition()->getY()],
            'playerName'       => $this->player->getName(),
            'playerAge'        => $this->player->getAge(),
            'playerHealth'     => $this->player->getHealth(),
            'playerMoney'      => $this->player->getMoney(),
            'playerExperience' => $this->player->getExperience(),
            'playerWeapons'    => $playerWeaponsData,
        ]);

        $this->gameJustSaved = true;
    }

    /**
     * Handles the user input and acts accordingly
     *
     * @param Map $map
     */
    protected function handleUserInput(Map $map): void
    {
        $action = IO::climate()->input('Where do you want to go? (w/a/s/d/info/help/save/quit):')->prompt();
        $futurePos = null;

        switch ($action) {
            case 'w':
                $futurePos = new Coordinate($this->player->getPosition()->getX(), $this->player->getPosition()->getY() - 1);
                break;
            case 's':
                $futurePos = new Coordinate($this->player->getPosition()->getX(), $this->player->getPosition()->getY() + 1);
                break;
            case 'a':
                $futurePos = new Coordinate($this->player->getPosition()->getX() - 1, $this->player->getPosition()->getY());
                break;
            case 'd':
                $futurePos = new Coordinate($this->player->getPosition()->getX() + 1, $this->player->getPosition()->getY());
                break;
            case 'info':
                $this->UI->showInfoScreen($this->player);
                break;
            case 'help':
                $this->UI->showHelpScreen();
                break;
            case 'save':
                try {
                    $this->saveGameState();
                    IO::climate()->lightGreen('Game saved!');
                } catch (RuntimeException $e) {
                    IO::climate()->error('Error while saving the game.');
                }

                IO::climate()->br();
                $this->handleUserInput($map);
                break;
            case 'quit':
                $this->confirmExitToMenu();
                break;
            default:
                IO::climate()->lightRed('Your input was not recognized.');
                IO::climate()->br();
                $this->handleUserInput($map);
        }

        $this->gameJustSaved = false;

        if ($futurePos !== null) {
            $contact = $map->willCollide($futurePos);

            if (!$contact) {
                $this->collidingEntity = null;
                $map->move($this->player, $futurePos);
            } elseif ($contact instanceof Entity) {
                $this->collidingEntity = $contact;
            }
        }
    }

    /**
     * Sets up the map & its objects
     */
    public function initWorld(): void
    {
        $this->map = new Map();
        $this->collidingEntity = null;
        $this->map->setAtCoords($this->player->getPosition(), $this->player);

        try {
            $this->NPCs = EntityFactory::generateNPCs(random_int(EntityFactory::$minGeneratedNPCs, EntityFactory::$maxGeneratedNPCs));
            $this->buildings = EntityFactory::generateBuildings(random_int(EntityFactory::$minGeneratedBuildings, EntityFactory::$maxGeneratedBuildings));
            $this->weapons = EntityFactory::generateWeapons(random_int(EntityFactory::$minGeneratedWeapons, EntityFactory::$maxGeneratedWeapons));
            $this->healthPills = EntityFactory::generateHealthPills(random_int(EntityFactory::$minGeneratedHealthPills, EntityFactory::$maxGeneratedHealthPills));
        } catch (\Exception $e) {
            $this->NPCs = EntityFactory::generateNPCs(mt_rand(EntityFactory::$minGeneratedNPCs, EntityFactory::$maxGeneratedNPCs));
            $this->buildings = EntityFactory::generateBuildings(mt_rand(EntityFactory::$minGeneratedBuildings, EntityFactory::$maxGeneratedBuildings));
            $this->weapons = EntityFactory::generateWeapons(mt_rand(EntityFactory::$minGeneratedWeapons, EntityFactory::$maxGeneratedWeapons));
            $this->healthPills = EntityFactory::generateHealthPills(mt_rand(EntityFactory::$minGeneratedHealthPills, EntityFactory::$maxGeneratedHealthPills));
        }

        $objects = array_merge($this->NPCs, $this->buildings, $this->weapons, $this->healthPills);

        foreach ($objects as $object) {
            $this->map->put($object);
        }
    }

    /**
     * isEnded getter
     *
     * @return bool
     */
    public function hasEnded(): bool
    {
        return $this->ended;
    }

    /**
     * @return Player
     */
    public function getPlayer(): Player
    {
        return $this->player;
    }

    /**
     * @param Player $player
     */
    public function setPlayer(Player $player): void
    {
        $this->player = $player;
    }

    /**
     * @param bool $inGame
     */
    public function setInGame(bool $inGame): void
    {
        $this->inGame = $inGame;
    }
}
