<?php

namespace CyberTerm\Classes;

use CyberTerm\Utils\IO;
use RuntimeException;

class UI
{
    /**
     * Shows the start screen
     * @param bool $showIntro
     */
    public function showStartScreen($showIntro): void
    {
        if ($showIntro) {
            sleep(2);
            IO::writeDialog('It\'s been a long time...');
            IO::writeDialog('I can still remember the last time I\'ve seen him.');
            IO::climate()->clear();
            sleep(2);

            try {
                IO::saveGame(['seenIntro' => true]);
            } catch (RuntimeException $e) {
                IO::climate()->out($e);
            }
        }

        IO::climate()->addArt(__DIR__ . '/../../resources/ASCII');
        IO::climate()->animation('logo')->speed(50)->enterFrom('bottom');
        IO::climate()->br(2);
        sleep(1);
        IO::writeDialog('                              Press ENTER to continue...', false);
    }

    /**
     * Shows the main menu and prompts the user
     *
     * @param bool $hasSaveGame
     * @return int
     */
    public function showMainMenu($hasSaveGame): int
    {
        IO::climate()->clear();
        IO::climate()->draw('logo');
        IO::climate()->border();
        IO::climate()->br();

        if ($hasSaveGame) {
            IO::climate()->lightGreen('                                    [1]   CONTINUE');
            IO::climate()->out('                                    ▬▬▬▬▬▬▬▬▬▬▬▬▬▬');
            IO::climate()->lightGreen('                                    [2]   NEW GAME');
            IO::climate()->out('                                    ▬▬▬▬▬▬▬▬▬▬▬▬▬▬');
            IO::climate()->lightRed('                                    [3]     QUIT');
        } else {
            IO::climate()->lightGreen('                                    [1]   NEW GAME');
            IO::climate()->out('                                    ▬▬▬▬▬▬▬▬▬▬▬▬▬▬');
            IO::climate()->lightRed('                                    [2]     QUIT');
        }

        IO::climate()->br();
        IO::climate()->border();
        IO::climate()->br();

        $input = IO::climate()->input('Please select your choice');
        $input->accept($hasSaveGame ? [1, 2, 3] : [1, 2], true);
        $response = $input->prompt();
        IO::climate()->br();

        return $response;
    }

    /**
     * Starts a new game
     * @param Game $game
     */
    public function startNewGame(Game $game): void
    {
        IO::climate()->clear();
        IO::climate()->animation('logo')->speed(50)->exitTo('top');
        IO::climate()->clear();
        sleep(2);

        IO::climate()->border();
        IO::climate()->lightGreen('CHAPTER 1 : Discovery');
        IO::climate()->border();
        IO::climate()->br(2);
        sleep(2);

        IO::climate()->lightGreen('anon@ctower.ctrl ~');
        IO::climate()->out('$ ctower -c ask_permission_to_land');
        sleep(2);
        IO::climate()->lightRed('>Please enter your identity');
        sleep(1);
        IO::climate()->br(2);
        IO::writeDialog('Oh well, here we go again.');
        IO::climate()->br();

        $playerName = IO::climate()->input('>Your name ?')->prompt();

        // Check if user input name is not empty
        while (empty($playerName)) {
            IO::climate()->lightRed('>Your name can\'t be empty.');
            IO::climate()->br();
            $playerName = IO::climate()->input('>Your name ?')->prompt();
        }

        $playerAge = IO::climate()->input('>Your age ?')->prompt();

        // Check if user input age is a number and is a positive number
        while (!is_numeric($playerAge) || $playerAge < 0) {
            IO::climate()->lightRed('>Your age must be a positive number.');
            IO::climate()->br();
            $playerAge = IO::climate()->input('>Your age ?')->prompt();
        }

        $game->setPlayer(new Player($playerName));
        $game->getPlayer()->setAge($playerAge);

        try {
            $game->saveGameState();
        } catch (RuntimeException $e) {
            IO::climate()->out($e);
        }

        IO::climate()->br();
        sleep(2);
        IO::climate()->lightGreen(">Welcome, {$game->getPlayer()->getName()}.");
        IO::climate()->br(2);

        IO::writeDialog('Where did I even land?');
        sleep(2);
        IO::climate()->br();

        IO::climate()->lightGreen('anon@ctower.ctrl ~');
        IO::climate()->out('$ ctower -c geoloc');
        sleep(2);
        IO::climate()->lightRed('>Coords: 35.68232, 139.78146 ; City: Gruden ; District: Nihon town ; Last activity: 22 days');
        sleep(2);
        IO::climate()->br();
        IO::writeDialog('There seems to be some enemy activity around here, I should get rid of it.');
        IO::climate()->br();
        IO::writeDialog('But first I need to find a weapon. I should get going.');

        $game->setInGame(true);
    }

    /**
     * Shows the end game screen
     */
    public function showEndGameScreen(): void
    {
        sleep(2);
        IO::writeDialog('Well, now that this area is clear, I can now access the next one.');
        IO::climate()->br();
        IO::climate()->lightGreen('You have finished the demo of CyberTerm! Press enter to go back to the main menu.');
        IO::climate()->input('')->prompt();
    }

    /**
     * Shows the player death screen
     *
     * @param Player $player
     */
    public function showDeathScreen(Player $player): void
    {
        IO::climate()->clear();
        IO::climate()->animation('death')->speed(50)->enterFrom('bottom');
        $currentYear = 2264;
        $bornYear = $currentYear - $player->getAge();
        $RIP = str_pad('RIP ' . $player->getName() . ' (' . $bornYear . ' - ' . $currentYear . ')', 60, ' ', STR_PAD_BOTH);
        IO::climate()->lightRed($RIP);

        if ($player->getLastWords()) {
            IO::climate()->br();
            IO::climate()->lightYellow("{$player->getName()}'s last words were: \"{$player->getLastWords()}\"");
        }

        IO::climate()->br();
        IO::writeDialog('Press Enter to go back to main menu', false);
    }

    /**
     *  Show help panel
     */
    public function showHelpScreen(): void
    {
        do {
            IO::climate()->clear();
            IO::climate()->draw('help');
            IO::climate()->br();
            $input = IO::climate()->red()->bold()->confirm('Back to the map?');
        } while (!$input->confirmed());
    }


    /**
     *  Show info panel
     * @param $player
     */
    public function showInfoScreen(Player $player): void
    {
        do {
            IO::climate()->clear();
            IO::climate()->draw('info');
            $logoHealth = str_pad('*   <light_magenta>♥</light_magenta> : ', 30, ' ');
            $playerHealth = str_pad("{$player->getHealth()} ", 25, ' ');
            $money = str_pad(" Money : ", 20, ' ', STR_PAD_LEFT);
            $playerMoney = str_pad("{$player->getMoney()}    *", 25, ' ');

            IO::climate()->out($logoHealth . $playerHealth . $money . $playerMoney);
            IO::climate()->white("*                                                          *");
            IO::climate()->white("************************************************************");
            IO::climate()->white("*                     List of weapons                      *");
            IO::climate()->white("************************************************************");

            if (count($player->getWeapons()) !== 0) {
                foreach ($player->getWeapons() as $index => $weapon) {
                    IO::climate()->white("*                                                          *");
                    IO::climate()->white("*   ¬  " . $weapon->getName() . " - Damage : " . $weapon->getDamage());
                }
                IO::climate()->white("*                                                          *");
            } else {
                IO::climate()->white("*                                                          *");
                IO::climate()->white("*                       No weapons                         *");
                IO::climate()->white("*                                                          *");
            }
            IO::climate()->white("************************************************************");
            IO::climate()->br();
            $input = IO::climate()->red()->bold()->confirm('Back to the map?');
        } while (!$input->confirmed());
    }
}
