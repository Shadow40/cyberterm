<?php

namespace CyberTerm\Interfaces;

interface Displayable
{
    /**
     * Returns the colored player icon to be shown on map
     *
     * @return string
     */
    public function getPrintCharacter(): string;

    /**
     * __toString override
     *
     * @return string
     */
    public function __toString();
}
