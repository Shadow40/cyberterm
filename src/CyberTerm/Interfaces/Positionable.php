<?php

namespace CyberTerm\Interfaces;

use CyberTerm\Classes\Coordinate;

interface Positionable
{
    /**
     * Gets the entity's position
     *
     * @return Coordinate
     */
    public function getPosition(): Coordinate;

    /**
     * Sets the entity's position
     *
     * @param Coordinate $newPosition
     */
    public function setPosition(Coordinate $newPosition): void;

    /**
     * Move the entity's position
     *
     * @param Coordinate $difference
     */
    public function move(Coordinate $difference): void;
}
