# CyberTerm

![Cyberterm logo](./docs/images/cyberterm_logo.jpg "Cyberterm logo")

A PHP cyberpunk RPG terminal game made by Julien Vanelian and Jessica Ferreira.

## Lore
Your father, Pikes Gelspie, was a known space cowboy, until he disappeared one night. Multiple clues took you on Unves, a war torn planet.
Your quest is to find him.

## Requirements
* PHP 7.1
* Composer

## Setup
Run ```composer install``` to generate autoload files.

Finally run ```php game.php``` to play the game!

For a great user experience, make sur your terminal window has at least 90 columns.

## Credits
ASCII art: [asciiart.eu](asciiart.eu), [patorjk.com](http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Type%20Something%20)